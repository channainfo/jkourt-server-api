module Api::V1
	class BaseController < ::ApplicationController
	  doorkeeper_for :all

	  protected

	  def current_user
	    @current_user ||= User.find_by(id: doorkeeper_token.resource_owner_id)
	  end

	end
end