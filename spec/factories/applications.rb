FactoryGirl.define do
  factory :application, class: Doorkeeper::Application do
    sequence(:name) { |n| "application_name_#{n}" }
	  #redirect_uri 'urn:ietf:wg:oauth:2.0:oob'
	  redirect_uri 'http://localhost:3000/'
  end
end