require 'rails_helper'

describe Api::V1::RegistrationsController do
  describe 'POST #create' do
    let!(:application) { create :application } # OAuth application
    let!(:user)        { create :user }
    let!(:token)       { create :oauth_token, :application => application, :resource_owner_id => user.id }

    it 'responds with 201' do
      post :create, email: 'user@example.com', password: 'password', :access_token => token.token
      expect(response.status).to eq(201)
    end

    it 'returns the user as json' do
      post :create, format: :json, email: 'user@example.com', password: 'password', :access_token => token.token
      result = JSON.parse(response.body)
      expect(result["email"]).to eq("user@example.com")
    end
  end
end