module Api::V1
	class RegistrationsController < BaseController
	  def create
	    user = User.new filter_params
	    if user.save
	    	Doorkeeper::AccessToken.create :application => doorkeeper_token.application, 
	    																 :resource_owner_id => doorkeeper_token.resource_owner_id
	    end

	    render json: user, status: 201
	  end

	  private
	  def filter_params
	    params.permit(:email, :password)
	  end
	end
end