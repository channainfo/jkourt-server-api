require 'rails_helper'
require 'acceptance/acceptance_helper'
require 'oauth2'

feature 'OAuth authorization' do
  let(:site) { "http://localhost:3001" }

  before(:each) do
    create(:user, email: 'user@example.com', password: 'password')
    app  = create(:application)

    @client = OAuth2::Client.new(app.uid, app.secret) do |oauth_client|
      oauth_client.request :url_encoded
      oauth_client.adapter :rack, Rails.application
    end
  end

  scenario 'auth ok' do
    
    token = @client.password.get_token("user@example.com", "password")
    token.should_not be_expired
  end

  scenario 'auth nok' do
    expect{ @client.password.get_token("user@example.com", "123")}.to raise_error(OAuth2::Error)
  end
end