# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
demo_user = User.create(email: 'demo@jkout.org', password: '123456')
client_user = User.create(email: 'client@jkout.org', password: '123456')
Doorkeeper::Application.create(name: 'example.com',
															redirect_uri: 'http://example.com',
															owner_id: client_user.id, owner_type: :user)
