class User < ActiveRecord::Base
  has_secure_password

  has_many :oauth_applications, class_name: 'Doorkeeper::Application', as: :owner

  def self.authenticate(email, password)
    begin
      user = User.find_by!(email: email)
      user.authenticate(password)
    rescue
      false
    end
  end
end
