require 'rails_helper'

describe User do
  describe ".authenticate" do
  	before(:each) do
  		@user = create(:user, email: 'user@example.com', password: 'password')
  	end

  	context "with correct email and password" do
  		it "return user instance" do
  			login_user = User.authenticate 'user@example.com', 'password'
  			expect(login_user).to eq @user
  		end
  	end

  	context "with incorrect user or password" do
  		it "return false" do
  			login_user = User.authenticate 'wrong@example.com', 'password'
  			expect(login_user).to eq false
  		end
  	end
  end
end
